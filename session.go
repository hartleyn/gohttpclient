package gohttpclient

import (
	"bytes"
	"io"
	"net/http"
	"net/url"
)

type Session struct {
	Client http.Client
	Header http.Header
}

func NewSession() Session {
	return Session{
		Client: http.Client{},
		Header: http.Header{},
	}
}

func (s Session) Get(requestURL string) ([]byte, int, error) {
	return s.sendRequest(http.MethodGet, requestURL, nil)
}

func (s Session) Post(requestURL string, data []byte) ([]byte, int, error) {
	return s.sendRequest(http.MethodPost, requestURL, data)
}

func (s Session) sendRequest(method string, requestURL string, data []byte) ([]byte, int, error) {
	reader := bytes.NewReader(data)
	body := io.NopCloser(reader)

	request, err := s.newRequest(method, requestURL, body)
	if err != nil {
		return nil, 0, err
	}

	res, err := s.Client.Do(request)
	if err != nil {
		return nil, 0, err
	}
	defer res.Body.Close()

	content, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, 0, err
	}
	return content, res.StatusCode, nil
}

func (s Session) newRequest(method string, requestURL string, body io.ReadCloser) (*http.Request, error) {
	parsedURL, err := url.Parse(requestURL)
	if err != nil {
		return nil, err
	}

	request := &http.Request{
		URL:    parsedURL,
		Header: s.Header,
		Method: method,
	}

	if body != nil {
		request.Body = body
	}

	return request, nil
}
